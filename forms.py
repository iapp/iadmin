#!/usr/bin/env python
from wtforms import Form, TextField, BooleanField, PasswordField, validators, widgets, IntegerField, SelectField, HiddenField

'''
DEFINICE FORMULARU
'''

class LoginForm(Form):
	login = TextField('Username', [
		validators.Length( min=4, max=25,
			message = "Username length must be betwean 4 and 25 chars.")
	])
	password = PasswordField('Password', [validators.Required(
		message="Please input your password.")
	])


class NewSiteForm(Form):
	vhost_name = TextField('ServerName', [
		validators.regexp('^[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$',
			message = "Invalid server name format")
		])
	vhost_alias	= TextField('ServerAlias')
	email = TextField('Email', [
		validators.Email(
			message = "Valid email format is mailbox@domain.tld" ),
	])
	login = TextField('User name', [
		validators.Required(
			message = "Must set login name|")
		])
	vhost_template = SelectField('Vhost template',
		choices = [])
	password = PasswordField('Password', [
		validators.regexp('.{7,30}',
			message = "Password must be at least 9 char long."),
	])
	active	= SelectField("Active",
		choices = [('Yes', 'Yes'), ('No', 'No')]
	)
	action = HiddenField('Action')


class NewDomainForm(Form):
	domain_name = TextField('Email domain', [
		validators.regexp('^[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$',
			message = "Domain name must be in FQDN format (example.com)")
		])
	domain_aliases = TextField('ServerAlias')
	quota = IntegerField('Quota (Hard)', [
		validators.number_range(max=10001,
			message = "Max hard quota is 10000"
	)])
	active	= SelectField("Active",
		choices = [('1', 'Yes'), ('0', 'No')]
	)
	action = HiddenField('Action')


class NewEmailForm(Form):
	email = TextField('Email', [
		validators.Email(
			message = "Must be valid email")
		])
	email_alias = TextField("Email aliases", [])
	domain_id = HiddenField('domain_id')
	domain = HiddenField('domain')
	quota = IntegerField('Quota (Hard)', [
		validators.number_range(max=10001,
			message = "Max hard quota is 10000 in MB"
	)])
	password = PasswordField('Password', [
		validators.regexp('.{9,30}',
			message = "Password must be at least 9 char long."),
	])
	comment	= TextField('Comment')
	active	= SelectField("Active",
		choices = [('1', 'Yes'), ('0', 'No')]
	)
	action = HiddenField('Action')
