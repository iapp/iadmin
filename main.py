#!/usr/bin/env python3
from flask import Flask, render_template, request, session, redirect, \
	url_for, g, jsonify
from flask.ext.pymongo import PyMongo
import json, os, imaplib
from pymongo import MongoClient
from bson import json_util
from lib import *
from datetime import timedelta
from forms import *


# init and config app
app = Flask(__name__)
app.config.from_pyfile('config.cfg')
"""
Working without mongo
try:
	MG = PyMongo(app)
except:
	print("Unable to connect mongo")
"""

@app.route("/logout")
def logout(id=None):
	session.pop('loged_in', None)
	return redirect("/")


@app.route("/login", methods=['GET', 'POST'])
def login():
	"""
	After successfull login session must contain
	these params:

		loged_in = True
		username = str()

	"""
	form = LoginForm(request.form)
	message = ""
	if request.method == 'POST' and form.validate():
		if imap_login(app.config['IMAP_SERVER'], str(form.login.data), str(form.password.data)):
			session['username'] = form.login.data
			session['loged_in'] = True
			return redirect('/')
		else:
			message = "Wrong password"

	return render_template("login.html", form=form, message = message)


@app.before_request
def check_privileges():
	"""
	Before access all urls except /login,
	check if logged_in is in session

	In future versions this function may provide access
	validation to parts of application

	"""
	if 'loged_in' not in session and request.endpoint not in ['login', 'static']:
		print('preposilam na prihlasovaci obrazovku')
		return redirect(url_for('login'))
	session.permanent = True
	app.permanent_session_lifetime = timedelta(minutes=10)

#  test data
APPS={
	'app1' : {
		'name' : 'application1',
		'git' : 'git.iapp.eu:masi/app1.git',
		'branch' : 'production',
		'image' : 'php',
		'url' : 'app1-marek.iapp.eu',
		'env' : {
			'DBNAME' : 'marek-app1',
			'DBPASS' : 'marekPassword1',
		},
		'state' : 'down',
		'comment' : 'PHP is dead, got o py, rb, js...',
		'log_path' : '/home/logs/marek/app1-latest.log'
	},
	'app2' : {
		'name' : 'application2',
		'git' : 'git.iapp.eu:masi/app2.git',
		'branch' : 'production',
		'image' : 'python3',
		'url' : 'python3-marek.iapp.eu',
		'env' : {
			'DBNAME' : 'marek-app2',
			'DBPASS' : 'marekPassword2',
		},
		'state' : 'running',
		'comment' : 'My own perfect flask application',
		'log_path' : '/home/logs/marek/app2-latest.log'
	},
	'app3' : {
		'name' : 'application3',
		'git' : 'git.iapp.eu:masi/app3.git',
		'branch' : 'production',
		'image' : 'AngularJS',
		'url' : 'angular-marek.iapp.eu',
		'env' : {
			'DBNAME' : 'marek-app3',
			'DBPASS' : 'marekPassword3',
		},
		'state' : 'running',
		'comment' : 'I am trying to learn angularJS, but it is not simple',
		'log_path' : '/home/logs/marek/app3-latest.log'
	},
}


@app.route("/api/", methods = ['GET'])
@app.route("/api/<app_name>", methods = ['GET'])
def api(app_name=""):
	''' REST api
	 	- simple REST api for frontend jquery application
	'''
	_data = {
		'app_list' : [str(i) for i in APPS.keys()],
		'login' : session['username']
	}
	if str(app_name) in APPS.keys():
		_data['data'] = APPS[app_name]
	return jsonify(_data)

@app.route("/")
def dashboard():
	return render_template('dashboard.html',
				data="")
"""
# MONGO backend
@app.route("/backupdb")
def backupdb():
	ret = {}
	for coll in MG.db.collection_names():
		if coll != 'system.indexes':
			ret[coll] = []
			for item in MG.db[coll].find():
				del item['_id']
				ret[coll].append(item)

	pprint(ret)
	with open(app.config['BKP_FILE'], "w") as F:
		json.dump(ret, F, sort_keys=True, indent=2)
	return str(ret)

# MONGO backend
@app.route("/restoredb")
def restoredb():
	with open(app.config['BKP_FILE'], 'r') as F:
		for _name, _data in json.load(F).items():
			print("##########  INIT ", _name, " #########")
			try:
				# drop collections
				MG.db[_name].drop()
			except:
				pass

			MG.db.create_collection(_name)
			for l in _data:
				MG.db[_name].insert(l)

	return str(MG.db.collection_names())
"""

if __name__ == "__main__":
	app.run()
