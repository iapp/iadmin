// funkce pro rozbalovani a zabalovani detailu u polozky
// <style> .hidden { display: none; };
// <div class='item'>
//  <div class='title'> Nadpis polozky </div>
//  <div class='detail hidden'> Nejaky detail k polozce vyse, muze zde byt obsazeno asi cokoli </div>
// </div>
//
$(".item .title").click(function() {

	//$title = $(this);
	$detail = $(this).next();

	if ( $(".detail").hasClass("hidden") ) {
		$(".detail").addClass('hidden');
	};

	console.log($detail);
	$detail.toggleClass("hidden");

});

function load_api_data(url="", action="") {
	/*
			My first rest api frontend application

			Use Flask as backend for serving data and run tasks

			/api/ 						- list of apps, and info about loged user
			/api/$app_name	  - get data about app
			/api/$app_nam/$action
												- actions:
													- create  need json data
													- delete  don't require parameters
													- update  need json data

	*/
	location = String("#" + url);
	var api_url = "/api/" + String(url);
	if (action.length > 1) {
		var api_url = api_url + String(action);
	}
	//else {
	//	var api_url = "/api/" + String(url) + ;
	//}
	console.log(api_url);

	$.getJSON( api_url, function( data ) {
		var apps = ['<option value="">Dashboard</option>'];

		// Generate app menu list
		$.each( data.app_list, function( key, val ) {
			apps.push("<option value=\"" + val + "\">" + val + "</option>");
		});

		// place data to page
		$("#select-app").html(apps);
		$("#view-data").html("")
		if ( url.length > 1 ) {
			$('#app-template').tmpl( data.data ).prependTo( '#view-data' );
		} else {
			$("#view-data").html("Select application")
		}
	});
};



function confirm_changes(message, changed) {
	return confirm( message + "\n" + changed.join() );
}


// Funkce pro teplomer v hlavnim menu, ktery vizualizuje expirovani prihlasovaci sesny
// a po vyprseni provede presmerovani na vychozi stranku.
//
setInterval(function(){
    var cur_size = $('.expire').width();
	var step = $('body').width()/630;
    if (cur_size > step) {
        $('.expire').width(cur_size - step);
		//console.log(cur_size);
    }
	else {
		window.location.replace("/");
	}}, 1000);

$( window ).load(function() {
	load_api_data(url="");
});
