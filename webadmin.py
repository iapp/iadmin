#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml, glob
from jinja2 import Template
from lib import yaml_load, yaml_edit, yaml_delete
from pwd import getpwnam

class webAdmin:
	'''
	
	
	'''
	def __init__(self, username, conf):
		self.conf = conf
		self.username = username
		self.uid = getpwnam(username).pw_uid
		self.action_uid = self.uid if self.username not in self.conf['admin_list'] else '*'
		self.get_sites()

	def vhost_generate(self, data):
		try:
			with open(template_path, 'r') as TPL:
				tpl = Template(TPL.read())
				return(tpl.render(**data))
		except IOError:
			logError("Error: ", template_path)


	def get_sites(self):
		""" vraci jako slovnik, kde klicem je vhost_name
		"""
		self._sites = dict()
		p = self.conf['config_path'].format(id=self.action_uid, vhost_name='*')
		for s in glob.glob(p):
			_d = yaml_load(s)
			_d['path'] = s
			self._sites[_d['vhost_name']] = _d
	
		return self._sites

	def get_sites_as_list(self):
		""" zatim je to treba delat takle blbe pro starou verzi UI
		"""
		_x = []
		for k, v in self.get_sites().items():
			_x.append(v)
		return _x

	def save(self, data):
		""" uklada data do yaml souboru
		"""
		yaml_edit(self.conf['config_path'].format(id=self.uid,\
											vhost_name=data['vhost_name']),
				data)
	
	def delete(self, data):
		return yaml_delete(self._sites[data['vhost_name']]['path'])
	

class Site:
	'''
	Az dojde na refaktorizaci slo by se cestou objektizace stranek a databazi,
	asi by se dedilo z tridy webadmin
	'''
	def __init__(self, conf_path):
		
		if conf_path is None and path.isfile(conf_path):
			self.conf_path = conf_path
			self.config = yaml_load(conf_path)
		else:
			self.config = {}

	
	
