#!/usr/bin/env python3
import yaml, glob, syslog
from jinja2 import Template
from os import system, remove, path
from pprint import pprint
import imaplib


def logErr(message):
	try:
		print("ERROR: ", message)
		syslog.syslog(syslog.LOG_ERR, message)
	except ImportError:
		print("import syslog is required")

def logInfo(message):
	try:
		print(message)
		syslog.syslog(syslog.LOG_INFO, message)
	except ImportError:
		print("import syslog is required")

def logDebug(message):
	try:
		syslog.syslog(syslog.LOG_DEBUG, message)
	except ImportError:
		print("import syslog is required")


def imap_login(hostname, username, password):
	try:
		_imap = imaplib.IMAP4(hostname, 143)
		if _imap.login(username, password):
			return True
	except:
		return False
	return False

def save_yaml(file_path, data):
	with open(file_path, 'w') as F:
		#F.write( yaml.dump(data, default_flow_style=False))
		yaml.dump(data, F, default_flow_style=False, explicit_start=True)

def yaml_edit(file_path, data):
	if path.isfile(file_path):
		# pokud jiz cesta existuje, natahnu stara data a rozsirim, upravim je novymi daty
		try:
			with open(file_path, 'r') as src_f:
				old_data = yaml.load(src_f)
				for k, v in data.items():
					if v is None:
						print("Odebiram ", old_data.pop(k, "Nenalezeno"))
					else:
						old_data[k] = v
				data = old_data
			return True

		except TypeError as Err:
			print("Vyjimka", Err)
			return False

	# v kazdem pripade zapisu
	print(data)
	with open(file_path, 'w+') as out_f:
		yaml.dump(data, out_f, default_flow_style=False, explicit_start=True)

def yaml_delete(file_path):
	try:
		print(file_path)
		return remove(file_path)
	except IOError as ERR:
		print(path.abspath('.'))
		print(ERR)
		return False


def yaml_load(file_path):
	try:
		with open(file_path, 'r') as F:
			return yaml.load(F)
	except ImportError:
		logError("import yaml is required")
		exit(1)

def template_generate(template_path, data):
	try:
		with open(template_path, 'r') as TPL:
			tpl = Template(TPL.read())
			return(tpl.render(**data))
	except IOError:
		logErr("Error: ", template_path)


def execute(command, data = None, dry_run = False):
	if type(data) is type(dict()):
		tpl = Template(command)
		cmd = tpl.render(**data)
		logDebug(cmd)
		print(cmd)
		if not dry_run:
			system(cmd)
		else:
			print(cmd)
	else:
		system(command)
