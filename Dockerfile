#
# Flask easyadmin docker container
#
#   - use last flask with python3
#   - code deploy is provide by git
#   - include mongodb
#
#
# based on fedora:latest
#

FROM fedora:latest
MAINTAINER Marek Sirovy "msirovy@gmail.com"
#RUN echo "nameserver 8.8.8.8 > /etc/resolv.conf"
RUN cat /etc/resolv.conf
#RUN dnf update -y
RUN dnf install -y gcc python3-pip python3-devel python3 git mongodb-server
COPY mongodb.conf /etc/mongod.conf
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
RUN /usr/bin/mongod -f /etc/mongod.conf --smallfiles --noauth

# listen on port 5000
EXPOSE 5000
ENTRYPOINT ["python3"]
CMD ["main.py"]

# END
